"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const forecast = fs_1.default.readFileSync('./forecast_data.json', 'utf-8');
const jsonForecast = JSON.parse(forecast);
console.log(jsonForecast.temperature);
jsonForecast.temperature = -30;
console.log(jsonForecast.temperature);
fs_1.default.writeFileSync('./forecast_data.json', JSON.stringify(jsonForecast, null, 4), 'utf-8');
